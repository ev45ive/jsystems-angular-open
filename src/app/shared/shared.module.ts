import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClockComponent } from './clock/clock.component';
import { RecentSearchesWidgetComponent } from './containers/recent-searches-widget/recent-searches-widget.component';
import { TrackListComponent } from './components/track-list/track-list.component';



@NgModule({
  declarations: [
    ClockComponent,
    RecentSearchesWidgetComponent,
    TrackListComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
  ],
  exports: [
    ClockComponent,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RecentSearchesWidgetComponent,
    TrackListComponent,
  ]
})
export class SharedModule { }
