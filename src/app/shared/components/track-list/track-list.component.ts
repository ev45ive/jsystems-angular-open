import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Track } from 'src/app/core/model/Album';

@Component({
  selector: 'app-track-list',
  templateUrl: './track-list.component.html',
  styleUrls: ['./track-list.component.scss']
})
export class TrackListComponent implements OnInit {

  @Input() tracks: Track[] = [];
  @ViewChild('audioRef') audioRef?: ElementRef<HTMLAudioElement>

  currentTrack?: Track

  play(track: Track) {
    this.currentTrack = track;
    setTimeout(() => {
      if (this.audioRef) {
        this.audioRef.nativeElement.volume = 0.1
        this.audioRef.nativeElement.play()
      }
    })
  }


  constructor() { }

  ngOnInit(): void {
  }

}
