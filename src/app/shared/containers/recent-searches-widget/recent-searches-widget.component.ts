import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from 'src/app/core/services/music-search.service';

@Component({
  selector: 'app-recent-searches-widget',
  templateUrl: './recent-searches-widget.component.html',
  styleUrls: ['./recent-searches-widget.component.scss']
})
export class RecentSearchesWidgetComponent implements OnInit {

  open = false

  queries: string[] = []

  constructor(private service: MusicSearchService) { }

  search(query:string){
    this.service.search(query)
  }

  ngOnInit(): void {

    this.service.queryChange.subscribe({
      next: query => {
        this.queries.push(query)
        this.queries = this.queries.slice(-3)
      }
    })
  }

}
