import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentSearchesWidgetComponent } from './recent-searches-widget.component';

describe('RecentSearchesWidgetComponent', () => {
  let component: RecentSearchesWidgetComponent;
  let fixture: ComponentFixture<RecentSearchesWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecentSearchesWidgetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentSearchesWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
