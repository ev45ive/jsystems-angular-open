import { ChangeDetectionStrategy, ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {

  time = new Date().toLocaleTimeString()

  constructor(
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef) {
    // cdr.detach()
  }

  ngOnInit(): void {
    // this.cdr.detectChanges()
    
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        this.time = new Date().toLocaleTimeString()
        this.cdr.detectChanges()
      }, 1_000)
    })
  }

}
