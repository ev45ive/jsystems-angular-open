import { Component } from '@angular/core';

@Component({
  selector: 'app-root, .lubie-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'MusicApp';
  open = false
  ngOnInit() {
  }
}
