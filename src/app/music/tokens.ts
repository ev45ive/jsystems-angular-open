import { InjectionToken } from '@angular/core';
import { AlbumItem } from '../core/model/Album';

const tokens = {};
export const API_URL = new InjectionToken<string>('Api URL');
export const INITIAL_ALBUMS = new InjectionToken<AlbumItem[]>('INITIAL_ALBUMS');
