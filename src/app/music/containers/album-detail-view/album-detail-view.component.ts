import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter, map, switchMap } from 'rxjs';
import { Album } from 'src/app/core/model/Album';
import { MusicSearchService } from 'src/app/core/services/music-search.service';

@Component({
  selector: 'app-album-detail-view',
  templateUrl: './album-detail-view.component.html',
  styleUrls: ['./album-detail-view.component.scss']
})
export class AlbumDetailViewComponent implements OnInit {

  // album_id = '5Tby0U5VndHW0SomYO7Id7'
  album_id = this.route.queryParamMap.pipe(
    map(qp => qp.get('id')),
    filter(id => !!id)
  )
  // album?: Album;
  album = this.album_id.pipe(
    switchMap(id => this.service.fetchAlbumById(id!))
  )

  constructor(
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) { }

  ngOnInit(): void {
    // this.route.queryParamMap.pipe(
    //   map(qp => qp.get('id')),
    //   filter(id => !!id)
    // ).subscribe(id => {
    //   this.album_id = id!
    //   this.service.fetchAlbumById(this.album_id).subscribe(album => {
    //     this.album = album
    //   })
    // })

  }

}
