import { AlbumItem } from 'src/app/core/model/Album';

// https://github.com/boo1ean/casual
// https://github.com/marak/Faker.js/
export const mockAlbums: AlbumItem[] = [
  {
    id: '123', type: 'album', name: 'Album 123', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }
    ]
  },
  {
    id: '345', type: 'album', name: 'Album 345', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/400/400' }
    ]
  },
  {
    id: '456', type: 'album', name: 'Album 456', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/500/500' }
    ]
  },
  {
    id: '567', type: 'album', name: 'Album 567', images: [
      { height: 300, width: 300, url: 'https://www.placecage.com/c/300/300' }
    ]
  },
];
