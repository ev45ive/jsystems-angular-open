import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, connectable, EMPTY, Observable, Subject, Subscription } from 'rxjs';
import { catchError, concatMap, exhaustMap, filter, map, mergeMap, multicast, share, shareReplay, switchMap, takeUntil, tap } from 'rxjs/operators';
import { Album, AlbumItem } from 'src/app/core/model/Album';
import { MusicSearchService } from 'src/app/core/services/music-search.service';

@Component({
  selector: 'app-music-search-view',
  templateUrl: './music-search-view.component.html',
  styleUrls: ['./music-search-view.component.scss'],
  // providers: [MusicSearchService] // local service
})
export class MusicSearchViewComponent implements OnInit {
  query = this.service.queryChange

  results = this.route.queryParamMap.pipe(
    map(params => params.get('q')),
    filter((q): q is string => q !== null),
    switchMap(q => {
      this.service.search(q)
      return this.service.albums
      // return this.service.fetchAlbums(q)
    }),
    shareReplay())

  search(query: string) {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: { q: query }
    })
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) { }



  ngOnInit(): void {
  }

  // makeRequest(query: string) {
  //   return this.service.fetchAlbums(query).pipe(
  //     catchError(error => {
  //       this.message = error.message
  //       return EMPTY
  //     })
  //   )
  // }
}
