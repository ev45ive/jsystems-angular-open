import { Component, Input, OnInit } from '@angular/core';
import { AlbumItem } from 'src/app/core/model/Album';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  @Input() results: AlbumItem[] | null = [];

  constructor() { }

  ngOnInit(): void {
  }

}
