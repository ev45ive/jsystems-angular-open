import { Component, Input, OnInit } from '@angular/core';
import { AlbumItem } from 'src/app/core/model/Album';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  @Input() album!: AlbumItem;

  constructor() { }

  ngOnInit(): void {
  }

}
