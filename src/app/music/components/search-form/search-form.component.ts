import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @Output() search = new EventEmitter<string>();
  extras = false

  @Input() query = ''

  ngOnChanges(changes: SimpleChanges): void {
    (this.searchForm.get('query') as FormControl).setValue(
      changes['query'].currentValue, {
        emitEvent:false // prevent emiting valueChange
      }
    )
  }

  searchForm = new FormGroup({
    'query': new FormControl('batman', []),
    'extra': new FormGroup({
      'type': new FormControl('album', []),
    })
  })

  constructor() {
    (window as any).form = this.searchForm

    this.submit.pipe(
      map(() => this.searchForm.get('query')!.value)
    ).subscribe(this.search)

  }

  submit = new EventEmitter()

  ngOnInit(): void {
    const field = this.searchForm.get('query')!

    field.valueChanges.pipe(
      debounceTime(400),
      filter(query => query.length >= 3),
      distinctUntilChanged(),
    )
      .subscribe(this.search)
    // .subscribe({
    //   next: q => this.search.next(q),
    //   error: q => this.search.error(q),
    //   complete: () => this.search.complete(),
    // })

  }

}
