import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music.component';
import { MusicSearchViewComponent } from './containers/music-search-view/music-search-view.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { environment } from 'src/environments/environment';
import { API_URL, INITIAL_ALBUMS } from './tokens';
import { MusicSearchService } from '../core/services/music-search.service';
import { SharedModule } from '../shared/shared.module';
import { AlbumDetailViewComponent } from './containers/album-detail-view/album-detail-view.component';

@NgModule({
  declarations: [
    MusicComponent,
    MusicSearchViewComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent,
    AlbumDetailViewComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MusicRoutingModule
  ],
  providers: [

    // {
    //   provide: MusicSearchService,
    //   useFactory(albums: AlbumItem[], url: string) {
    //     return new MusicSearchService(albums, url)
    //   },
    //   deps: [INITIAL_ALBUMS, API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    //   // deps: [INITIAL_ALBUMS, API_URL]
    // },
    // MusicSearchService
  ]
})
export class MusicModule { }
