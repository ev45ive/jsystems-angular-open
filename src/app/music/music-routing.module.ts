import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumDetailViewComponent } from './containers/album-detail-view/album-detail-view.component';
import { MusicSearchViewComponent } from './containers/music-search-view/music-search-view.component';
import { MusicComponent } from './music.component';

const routes: Routes = [{
  /// /music
  path: '', component: MusicComponent,
  children: [
    /// /music/ -> /music/search
    {
      path: '', redirectTo: 'search', pathMatch: 'full'
    },
    {
      /// /music/search
      path: 'search',
      component: MusicSearchViewComponent
    },
    {
      /// /music/albums?id=....
      path: 'albums',
      component: AlbumDetailViewComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }
