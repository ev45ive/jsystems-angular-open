import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../core/model/Playlist';

type Modes = 'details' | 'edit' | 'create';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: Modes = 'details'

  playlists: Playlist[] = [
    {
      id: '123',
      name: 'Playlist 123',
      public: true,
      description: 'Best playlist 123', type:'playlist'
    },
    {
      id: '234',
      name: 'Playlist 234',
      public: true,
      description: 'Best playlist 234', type:'playlist'
    },
    {
      id: '345',
      name: 'Playlist 345',
      public: true,
      description: 'Best playlist  345', type:'playlist'
    }
  ]

  selected?: Playlist //= this.playlists[2]

  selectPlaylist(id: Playlist['id']) {
    this.selected = this.playlists.find(p => p.id == id)
  }

  constructor() { }

  cancel() { this.mode = 'details' }

  edit() { this.mode = 'edit' }

  save(draft: Playlist) {
    this.selected = draft;
    this.mode = 'details'
    // Mutable change - ItemsList OnPush is NOT TRIGGERED !!!! no change detected!!!
    // const index = this.playlists.findIndex(p => p.id == draft.id)
    // if (index !== -1) {
    //   this.playlists[index] = draft
    // }
    // Immutable change - copy of Input - OnPush IS TRIGGERED // change detected!!
    this.playlists = this.playlists.map(p => p.id == draft.id ? draft : p)
  }

  ngOnInit(): void {
  }

}
