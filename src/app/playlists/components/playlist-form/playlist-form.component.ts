import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { CheckboxControlValueAccessor, NgForm } from '@angular/forms';
import { Playlist } from '../../../core/model/Playlist';

// CheckboxControlValueAccessor

@Component({
  selector: 'app-playlist-form',
  templateUrl: './playlist-form.component.html',
  styleUrls: ['./playlist-form.component.scss']
})
export class PlaylistFormComponent implements OnInit {

  @Input() playlist!: Playlist
  // draft!: Playlist

  @Output() save = new EventEmitter<Playlist>();
  @Output() cancel = new EventEmitter();

  cancelClick() {
    this.cancel.emit(this.playlist)
  }

  submit(form: NgForm) {

    const draft = {
      ...this.playlist,
      ...form.value
    }
    this.save.emit(draft)
  }
  
  //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
  constructor() { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    // console.log('ngOnInit');
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('ngOnChanges');
  }

}
