import { Album, Track } from "./Album"

export interface Playlist {
    id: string;
    name: string;
    public: boolean;
    description: string;
    type: 'playlist',
    tracks?: Track[]
}

type RespType = Playlist | Track | Album;

export function showLabel(res: RespType) {
    if (res.type == 'playlist') {
        return `${res.name} (${res.tracks?.length || 0} tracks)`;
    }
    switch (res.type) {
        case 'track': return `${res.name} (${res.duration_ms} ms)`
        case 'album': return `${res.name} (${res.total_tracks} tracks)`
        // default:
        //     const _never: never = res
        //     throw Error('Unexpected response ' + res)
        default: exhaustivenessCheck(res)
    }
}

// https://github.com/colinhacks/zod#objects
// https://github.com/gvergnaud/ts-pattern

function exhaustivenessCheck(never: never): never {
    throw Error('Unexpected data ' + never)
}

// const playlist: unknown = {}

// function validatePlaylist(resp: Playlist | {}) {
//     if ('id' in resp) {
//         resp
//     }
//     if('type' in resp && resp.type === 'playlist'){
//         resp
//     }
// }

// let x: any = 123
//     x = '123'
//     x.get().me.aMillionDollars().now()
// let y = x + 1

// let response: unknown // = ...api.fetch(...
// response = 123;
// response = '324';
// (response as Playlist).id


// let playlist = {} as Playlist | undefined;

// if (playlist !== undefined) {
//     playlist.id
// }
// playlist && playlist.id
// const sel1 = playlist?.id
// const sel2 = playlist!.id

