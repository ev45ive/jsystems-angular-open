import { EventEmitter, Inject, Injectable } from '@angular/core';
import { INITIAL_ALBUMS, API_URL } from 'src/app/music/tokens';
import { Album, AlbumItem, AlbumsSearchResponse } from '../model/Album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from './auth/auth.service';
import { catchError, map, pluck, startWith, switchMap, tap } from 'rxjs/operators';
import { BehaviorSubject, concat, from, merge, of, ReplaySubject, Subject, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MusicSearchService {
  queryChange = new ReplaySubject<string>(3)
  albums = new BehaviorSubject(this.results)

  constructor(
    @Inject(INITIAL_ALBUMS) private results: AlbumItem[] = [],
    @Inject(API_URL) private api_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {
    (window as any).subject = this.albums;

    this.queryChange.pipe(
      switchMap(query => this.fetchAlbums(query)))
      .subscribe(albums => {
        this.albums.next(albums)
      })

  }

  // Command query separation
  search(query: string) {
    this.queryChange.next(query)
  }

  fetchAlbums(query: string) {

    return this.http.get<unknown>(`${this.api_url}search`, {
      params: { type: 'album', q: query },
      headers: {
        Authorization: `Bearer ${this.auth.getToken()}`
      },
    }).pipe(
      map(res => {
        validateAlbumsSearchResponse(res)
        return res;
      }),
      map(res => res.albums.items),
      // map(res => (res as any).placki.ala.ma.kota),
      catchError(error => {
        if (!(error instanceof HttpErrorResponse)) {
          return throwError(() => new Error('Unexpected error'))
        }
        const errorData = error.error;

        if (!isSpotifyError(errorData)) {
          return throwError(() => new Error('Unexpected server response'))
        }

        if (errorData.error.status === 401) {
          this.auth.login();
          return throwError(() => new Error('Refreshing token'))

        }


        return throwError(() => new Error(errorData.error.message))
      })
    )

  }


  fetchAlbumById(id: string) {

    return this.http.get<Album>(`${this.api_url}albums/${id}`, {
      headers: {
        Authorization: `Bearer ${this.auth.getToken()}`
      },
    }).pipe(
      catchError(error => {
        if (!(error instanceof HttpErrorResponse)) {
          return throwError(() => new Error('Unexpected error'))
        }
        const errorData = error.error;

        if (!isSpotifyError(errorData)) {
          return throwError(() => new Error('Unexpected server response'))
        }

        if (errorData.error.status === 401) {
          this.auth.login();
          return throwError(() => new Error('Refreshing token'))

        }


        return throwError(() => new Error(errorData.error.message))
      })
    )

  }
}

interface SpotifyError {
  error: { message: string, status: number }
}

function isSpotifyError(error: any): error is SpotifyError {
  return 'error' in error && 'message' in error.error && typeof error.error.message === 'string'
}

function validateAlbumsSearchResponse(res: any): asserts res is AlbumsSearchResponse {
  if (!('albums' in res && 'items' in res.albums && Array.isArray(res.albums.items))) {
    throw new Error('Invalid AlbumsSearchResponse')
  }
}