import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private oAuth: OAuthService
  ) {
    this.oAuth.configure(environment.authConfig)
  }

  async init() {
    await this.oAuth.tryLogin()
    if(!this.getToken()){
      this.login()
    }
  }

  login() {
    this.oAuth.initLoginFlow()
  }
  logout() {
    this.oAuth.logOut()
  }

  getToken() {
    return this.oAuth.getAccessToken()
  }
}
