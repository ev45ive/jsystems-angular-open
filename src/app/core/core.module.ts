import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { API_URL, INITIAL_ALBUMS } from '../music/tokens';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './services/auth/auth.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [
    // {
    //   provide: HttpHandler, useClass: MyBetterAwesmeHttpHandler
    // },
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    {
      provide: INITIAL_ALBUMS,
      useValue: []
    },
  ]
})
export class CoreModule {
  constructor(
    private auth: AuthService
  ) {
    this.auth.init()
  }
}
