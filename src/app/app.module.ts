import { ApplicationRef, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { MusicModule } from './music/music.module';
import { mockAlbums } from './music/containers/music-search-view/mockAlbums';
import { INITIAL_ALBUMS } from './music/tokens';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    CoreModule,
    /* -- */
    PlaylistsModule,
    // MusicModule,
    /* -- */
    SharedModule,
    AppRoutingModule,
    // MocksModule 
  ],
  providers: [
    {
      provide: INITIAL_ALBUMS,
      useValue: mockAlbums
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  // ngDobBootstrap(app:ApplicationRef){
  //   // getconfig.fromserver().then(()=>...
  //   app.bootstrap(AppComponent)
  // }
}
