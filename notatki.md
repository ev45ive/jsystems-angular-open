# GIT
cd ..
git clone https://ev45ive@bitbucket.org/ev45ive/jsystems-angular-open.git
cd jsystems-angular-open
npm i 
npm run start

## update
git stash -u 
git pull -f
 
<!-- lub -->
git pull -u origin master

# Instalacje
node -v 
v14.17.0

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

chrome

code -v 
1.61.2

## Extensions 
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template

https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode

https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

https://angular.io/guide/devtools

https://docs.emmet.io/cheat-sheet/

# Angular CLI

npm i -g @angular/cli 
ng --version
ng new --help

ng new angular-open --directory "." --style scss --routing --strict
ng new angular-open --directory .
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS   [ https://sass-lang.com/documentation/syntax#scss 

ng s -o
npm start -- -o

## Clock
ng g c clock
CREATE src/app/clock/clock.component.html (20 bytes)
CREATE src/app/clock/clock.component.spec.ts (619 bytes)
CREATE src/app/clock/clock.component.ts (272 bytes)     
CREATE src/app/clock/clock.component.scss (0 bytes)     
UPDATE src/app/app.module.ts (617 bytes)

## Zone.js
https://www.npmjs.com/package/zone.js?activeTab=readme
https://www.angular.love/2018/03/04/angular-i-zone-js/


## UI Toolkits
https://material.angular.io/components/grid-list/overview
https://github.com/angular/flex-layout
https://ng-bootstrap.github.io/#/components/buttons/examples
https://ng.ant.design/docs/schematics/en
https://primefaces.org/primeng/showcase/#/ 

# Angular 13 persitent build cache
https://angular.io/cli/cache

# Playlists

ng g m playlists --routing -m app

ng g c playlists/containers/playlists-view

ng g c playlists/components/playlist-list
ng g c playlists/components/playlist-list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form


# MOdule types

ng g m core -m app
ng g m feature1
ng g m feature2
ng g m feature3
ng g m shared -m app


# Search module
ng g m music --routing -m app --route "music"
ng g c music/containers/music-search-view
ng g c music/components/search-form
ng g c music/components/search-results
ng g c music/components/album-card


## Services

ng g s core/services/music-search


## Album details

ng g c music/containers/album-detail-view

TODO:
- Dodaj AlbumDetailViewComponent do music-router pod sciezka /music/albums/
- Pobierz i wyświetl parametr ?id=5Tby0U5VndHW0SomYO7Id7
- http://localhost:4200/music/albums?id=5Tby0U5VndHW0SomYO7Id7
- Widac na ekranie album id równy "5Tby0U5VndHW0SomYO7Id7"
- Wstrzyknac serwis MusicSearchService
- Wykonujemy zapytanie http uzywajac metody MusicSearchService.fetchAlbumById(5Tby0U5VndHW0SomYO7Id7)
- Subskrybujemy dane albumu
- po zaladowaniu danych wyswietlamy - id albumu, nazwa, karta albumu <app-album-card [album]="...dane..." >
+ lista utworów <app-track-list>
+ link z wynikow wyszukiwania do <a routerLink="/music/albums/" [queryParams]="{id: album.id}">

ng g c shared/components/track-list --export